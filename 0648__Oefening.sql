USE aptunes;

DELIMITER $$
-- Deze procedure maakt een willekeurig getal tussen 1 en 3 en geeft voor het resultaat een specifieke error-code
CREATE PROCEDURE DemonstrateHandlerOrder()
BEGIN
	DECLARE randomNumber INT DEFAULT 0;
    
	DECLARE CONTINUE HANDLER FOR SQLSTATE '45002'
    BEGIN
		SELECT 'State 45002 opgevangen. Geen probleem.' AS Bericht;
    END;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
		SELECT 'Een algemene fout opgevangen.' AS Bericht;
    END;
    
    SET randomNumber = FLOOR(RAND() * 4) + 1;
    
    CASE
		WHEN randomNumber = 1 THEN
			SIGNAL SQLSTATE '45001';
		WHEN randomNumber = 2 THEN
			SIGNAL SQLSTATE '45002';
		WHEN randomNuber = 3 THEN
			SIGNAL SQLSTATE '45003';
    END CASE;
END$$
DELIMITER ;