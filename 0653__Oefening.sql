USE aptunes;
DELIMITER $$
CREATE PROCEDURE GetAlbumDuration2(IN album INT, OUT totaalLengte SMALLINT UNSIGNED)
SQL SECURITY INVOKER
BEGIN
	DECLARE done INTEGER DEFAULT 0;
    DECLARE lengteLiedje TINYINT UNSIGNED;
    
    DECLARE ondervraagdLiedje CURSOR FOR
    SELECT Lengte FROM liedjes WHERE Albums_Id = album;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND
    SET done = 1;
    
    OPEN ondervraagdLiedje;
		telLengte: LOOP
			FETCH ondervraagdLiedje INTO lengteLiedje;
            IF done = 1 THEN
				LEAVE telLengte;
			END IF;
		SET totaalLengte = totaalLengte + lengteLiedje;
        END LOOP telLengte;
    CLOSE ondervraagdLiedje;
END$$
DELIMITER ;