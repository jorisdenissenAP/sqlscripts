USE ModernWays;
SELECT min(PersoonlijkGemiddelde) AS 'Gemiddelde'
FROM (SELECT avg(cijfer) AS 'PersoonlijkGemiddelde' FROM evaluaties
GROUP BY studenten_id) as PersoonlijkGemiddelde;