USE modernways;
CREATE TABLE IF NOT EXISTS student(
Studentennummer INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);

CREATE TABLE IF NOT EXISTS Opleiding(
Naam VARCHAR(100) CHAR SET utf8mb4 PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS Vak(
Naam VARCHAR(100) CHAR SET utf8mb4 PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS Lector(
Personeelsnummer INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
Voornaam VARCHAR(100) CHAR SET utf8mb4,
Familienaam VARCHAR(100) CHAR SET utf8mb4
);

ALTER TABLE student
ADD COLUMN Semester TINYINT UNSIGNED NOT NULL,
ADD COLUMN Opleiding_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
ADD CONSTRAINT fk_student_opleiding FOREIGN KEY (Opleiding_Naam) REFERENCES Opleiding(Naam);

CREATE TABLE IF NOT EXISTS Geeft(
Lector_personeelsnummer INT NOT NULL,
Vak_naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
CONSTRAINT fk_geeft_lector FOREIGN KEY (lector_personeelsnummer) REFERENCES Lector(personeelsnummer),
CONSTRAINT fk_geeft_vak FOREIGN KEY (Vak_naam) REFERENCES Vak(Naam)
);

CREATE TABLE IF NOT EXISTS Opleidingsonderdeel(
Opleiding_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Vak_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Semester TINYINT UNSIGNED,
CONSTRAINT fk_opleidingsonderdeel_opleiding FOREIGN KEY (Opleiding_Naam) REFERENCES Opleiding(Naam),
CONSTRAINT fk_opleidingsonderdeel_vak FOREIGN KEY (Vak_Naam) REFERENCES Vak(Naam)
);
