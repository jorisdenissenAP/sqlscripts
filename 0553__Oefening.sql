USE modernways;
SET SQL_SAFE_UPDATES = 0;
UPDATE baasjes
SET Huisdieren_id = 1
WHERE Naam = 'Vincent';

UPDATE baasjes
SET Huisdieren_id = 2
WHERE Naam = 'Christiane';

UPDATE baasjes
SET Huisdieren_id = 3
WHERE Naam = 'Esther';

UPDATE baasjes
SET Huisdieren_id = 4
WHERE Naam = 'Bert';

UPDATE baasjes
SET Huisdieren_id = 5
WHERE Naam = 'Lyssa';

SET SQL_SAFE_UPDATES = 1