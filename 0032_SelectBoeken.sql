USE ModernWays;
-- eerst wordt een boek ingevoegd
INSERT INTO Boeken (Voornaam,Familienaam,Titel, Categorie)
VALUES ('Mathijs','Degrote','Leren werken met SQL', 'Programmeren');
-- er gaat wat tijd voorbij en ik weet niet meer of het "Mathijs" of "Matijs" is
-- ik los dit op met LIKE
SELECT Voornaam from Boeken
   WHERE Voornaam LIKE 'ma%ijs';