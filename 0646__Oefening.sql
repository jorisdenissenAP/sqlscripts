USE aptunes;

DELIMITER $$
-- Deze procedure maakt meerdere nep-data tegelijk aan in de tabel 'albumreleases'
CREATE PROCEDURE MockAlbumReleases(IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT 0;
    
    -- Deze repeat zal data blijven toevoegen tot het gevraagde aantal is bereikt
    REPEAT
		-- Roep een eerder werkende procedure op
        CALL MockAlbumReleaseWithSuccess();
        SET counter = counter + 1;
    UNTIL counter = extraReleases
    END REPEAT;
END$$
DELIMITER ;