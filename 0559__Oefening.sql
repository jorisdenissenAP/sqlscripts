USE modernways;
ALTER TABLE liedjes
ADD COLUMN artiest_id INT,
ADD CONSTRAINT FOREIGN KEY (artiest_id) REFERENCES artiesten(id)