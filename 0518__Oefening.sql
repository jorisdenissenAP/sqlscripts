use modernways;
ALTER TABLE huisdieren ADD COLUMN Geluid varchar(20) char set utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET geluid = 'WAF!'
WHERE soort = 'hond';
UPDATE huisdieren SET geluid = 'miauwww...'
WHERE soort = 'kat';
SET SQL_SAFE_UPDATES = 1;