USE aptunes;

CREATE INDEX FamilienaamVoornaamIdx
ON muzikanten(familienaam, voornaam)

-- Dit script is overeenkomstig met de index-creatie als in de modeloplossing
-- ook weer buiten de 'USE aptunes;' lijn na

-- gevraagde tests hadden de gevraagde uitkomst