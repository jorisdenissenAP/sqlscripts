USE aptunes;

CREATE INDEX VoornaamFamilienaamIdx
ON muzikanten(Voornaam(9), Familienaam(9));

-- heb het vergeleken met de modeloplossing na een foutmelding via mail
-- is EXACT hetzelfde op de 'USE aptunes;' lijn na
-- heb dus sindsdien niets veranderd

-- gaf goede resultaten tijdens een test in de database