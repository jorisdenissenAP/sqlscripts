USE aptunes;
DELIMITER $$
-- Deze precedure zoekt de titel van liedjes op die een bepaald meegegeven woord bevatten
CREATE PROCEDURE GetLiedjes(IN term VARCHAR(50))
BEGIN
	SELECT Titel
    FROM Liedjes
    WHERE Titel LIKE CONCAT('%', term, '%');
END$$
DELIMITER ;