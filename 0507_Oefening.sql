USE ModernWays;
CREATE TABLE Metingen(
Tijdstip DATETIME NOT NULL,
Grootte TINYINT UNSIGNED NOT NULL,
Marge FLOAT(3,2)
);