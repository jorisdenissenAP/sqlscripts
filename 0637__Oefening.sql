use ModernWays;
-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
use ModernWays;
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;

use ModernWays;
-- Voeg extra kolommen toe aan de tabel Personen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);


use ModernWays;
-- Zorg voor een niet-verplichte foreign van de boeken naar de auteurs met een id
alter table Boeken add Personen_Id int null;

-- Link de tabellen 'boeken' en 'personen' met elkaar
-- zet de id's van de juiste personen op hun plaats in 'boeken'
SET SQL_SAFE_UPDATES = 0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;

-- foreign key verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

-- verwijder de dubbele info over de auteurs uit 'boeken'
alter table Boeken drop column Voornaam,
    drop column Familienaam;
    
-- dan de constraint toevoegen
-- maak van 'Personen_Id' een effectieve foreign key
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);