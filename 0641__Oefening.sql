USE aptunes;
DELIMITER $$
-- deze procedure telt het aantal genres in de database en geeft deze mee
CREATE PROCEDURE NumberOfGenres(OUT aantal TINYINT)
BEGIN
	SELECT COUNT(*)
    INTO aantal
    FROM genres;
END$$
DELIMITER ;