/* Dit script laat zien of een view updatable is of niet.*/
/* Updatable wilt zeggen dat je in de view bepaalde waardes kan aanpassen */

/* Waardes die je niet kan aanpassen zijn bijvoorbeeld berekende waardes (SUM, AVG, ...)*/
/* Hier door wordt een view Not Updatable */

SELECT table_name, is_updatable
FROM information_schema.views
WHERE table_schema = 'ModernWays';