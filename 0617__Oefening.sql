USE modernways;

CREATE VIEW AuteursBoekenRatings
AS
SELECT auteursboeken.auteur AS 'Auteur', auteursboeken.titel AS 'Boeken', gemiddelderatings.rating
FROM gemiddelderatings
INNER JOIN auteursboeken ON gemiddelderatings.Boeken_Id = auteursboeken.Boeken_Id