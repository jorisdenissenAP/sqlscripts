USE modernways;

SELECT platformen.Naam AS 'Platform', releases.Games_Id AS 'Game'
FROM platformen
LEFT JOIN releases ON platformen.id = releases.platformen_id
WHERE Games_id IS NULL

UNION ALL

SELECT releases.Platformen_Id, games.titel AS 'Game'
FROM releases
RIGHT JOIN games ON releases.games_id = games.id
WHERE Platformen_id IS NULL