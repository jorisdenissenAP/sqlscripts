USE ModernWays;
SELECT studenten.id AS 'Id' FROM studenten
INNER JOIN evaluaties
ON studenten.id = evaluaties.studenten_id
GROUP BY studenten.id
HAVING avg(cijfer) > (SELECT avg(cijfer) FROM evaluaties)