USE modernways;
SET SQL_SAFE_UPDATES = 0;
UPDATE liedjes
SET artiest_id = 1
WHERE Artiest = 'Led Zeppelin';

UPDATE liedjes
SET artiest_id = 2
WHERE Artiest = 'The Doors';

UPDATE liedjes
SET artiest_id = 3
WHERE Artiest = 'Molly Tuttle';

UPDATE liedjes
SET artiest_id = 4
WHERE Artiest = 'Goodnight, Texas';

UPDATE liedjes
SET artiest_id = 5
WHERE Artiest = 'Layla Zoe';

UPDATE liedjes
SET artiest_id = 6
WHERE Artiest = 'Danielle Nicole';

UPDATE liedjes
SET artiest_id = 7
WHERE Artiest = 'Van Halen';

SET SQL_SAFE_UPDATES = 1;