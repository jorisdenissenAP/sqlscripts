USE Modernways;

CREATE VIEW AuteursBoeken
AS
SELECT CONCAT(personen.voornaam, ' ', personen.familienaam) AS "Auteur", boeken.titel AS "Titel"
FROM publicaties
INNER JOIN boeken ON Boeken_Id = boeken.Id
INNER JOIN personen ON Personen_Id = personen.id