USE aptunes;

CREATE INDEX VoornaamFamilienaamIdx
ON muzikanten(Voornaam, Familienaam)