USE ModernWays;
-- oplopend sorteren volgens familienaam
-- ascending
SELECT Voornaam, Familienaam, Titel FROM Boeken
   ORDER BY Familienaam ASC, Voornaam, Titel;