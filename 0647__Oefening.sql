USE aptunes;

DELIMITER $$
-- Deze procedure maakt meerdere nep-data tegelijk aan in de tabel 'albumreleases'
CREATE PROCEDURE MockAlbumReleasesLoop(IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT 0;
    
    -- Deze repeat zal data blijven toevoegen tot het gevraagde aantal is bereikt
    releaseLoop: LOOP
		-- Roep een eerder werkende procedure op
        CALL MockAlbumReleaseWithSuccess();
        SET counter = counter + 1;
		IF counter = extraReleases THEN
			LEAVE releaseLoop;
		END IF;
    END LOOP;
END$$
DELIMITER ;