USE modernways;
ALTER VIEW auteursboeken
AS
SELECT CONCAT(personen.voornaam, ' ', personen.familienaam) AS "Auteur", boeken.titel AS "Titel", boeken.Id AS "Boeken_Id"
FROM publicaties
INNER JOIN boeken ON Boeken_Id = boeken.Id
INNER JOIN personen ON Personen_Id = personen.id