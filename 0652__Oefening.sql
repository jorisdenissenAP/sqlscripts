USE aptunes;

CREATE USER student@localhost
IDENTIFIED BY 'ikbeneenstudent';

GRANT EXECUTE ON PROCEDURE aptunes.GetAlbumDuration
TO student@localhost;