USE aptunes;
DELIMITER $$
-- Deze procedure maakt nep-data aan in de tabel 'albumreleases'
CREATE PROCEDURE MockAlbumRelease()
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    -- Telt het aantal albums
    SELECT COUNT(*)
    INTO numberOfAlbums
    FROM albums;
    
    -- Telt het aantal artiesten
    SELECT COUNT(*)
    INTO numberOfBands
    FROM bands;
    
    -- Genereert een willekeurig id voor de albums en de artiesten
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
    
    -- Voegt de nep-data toe aan 'albumreleases' als de combinatie van die twee variabelen nog niet bestaat
    IF NOT (randomBandId, randomAlbumId) IN (SELECT * FROM albumreleases) THEN
		INSERT INTO albumreleases (Bands_Id, Albums_Id)
        VALUES (randomBandId, randomAlbumId);
    END IF;
END$$
DELIMITER ;