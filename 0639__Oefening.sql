-- Voeg een nieuwe auteur toe aan 'personen'
insert into Personen (
   Voornaam, Familienaam
)
values (
   'Jean-Paul', 'Sartre'
);

-- voeg het boek toe aan 'boeken'
-- en zorg voor de link boek-auteur
use ModernWays;
insert into Boeken (
   Titel,
   Stad,
   Verschijningsdatum,
   Commentaar,
   Categorie,
   Personen_Id
)
values (
   'De Woorden',
   'Antwerpen',
   '1962',
   'Een zeer mooi boek.',
   'Roman',
   (select Id from Personen where
       Familienaam = 'Sartre' and Voornaam = 'Jean-Paul'))