USE aptunes;

DELIMITER $$
CREATE PROCEDURE DangerousInsertAlbumreleases()
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    DECLARE counter INT DEFAULT 0;
    DECLARE randomNumber INT DEFAULT 0;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		ROLLBACK;
        SELECT 'Nieuwe releases konden niet worden toegevoegd.';
    END;
    
    START TRANSACTION;
		-- Telt het aantal albums
		SELECT COUNT(*)
		INTO numberOfAlbums
		FROM albums;
		
		-- Telt het aantal artiesten
		SELECT COUNT(*)
		INTO numberOfBands
		FROM bands;
		
		-- Genereert een willekeurig id voor de albums en de artiesten
		SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
		SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
		
		-- Voegt de nep-data toe aan 'albumreleases' als de combinatie van die twee variabelen nog niet bestaat
		releaseLoop: LOOP
			IF NOT (randomBandId, randomAlbumId) IN (SELECT * FROM albumreleases) THEN
				INSERT INTO albumreleases (Bands_Id, Albums_Id)
				VALUES (randomBandId, randomAlbumId);
				SET counter = counter + 1;
			END IF;
			
			-- Genereert de kans op een error
			IF counter = 2 THEN
				SET randomNumber = FLOOR(RAND() * 4) + 1;
			END IF;
			
			-- Checkt of er al 3 albums zijn toegevoegd
			IF counter = 3 THEN
				LEAVE releaseLoop;
			END IF;
			
			-- Checkt of er een "error" is voorgekomen
			IF randomNumber = 2 THEN
				SIGNAL SQLSTATE '45000';
			END IF;
		END LOOP;
    COMMIT;
END$$
DELIMITER ;