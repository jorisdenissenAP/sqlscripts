USE aptunes;

DELIMITER $$
-- Deze procedure maakt een nieuw album aan in de database en linkt ze met de betrokken artiest
CREATE PROCEDURE CreateAndReleaseAlbum(IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
	START TRANSACTION;
		-- Voegt het album toe aan de database
		INSERT INTO albums (albums.Titel)
        VALUES (titel);
        -- Koppelt de artiest en het nieuwe album door de band id en het album id in een nieuwe record
        -- van 'albumreleases' toe te voegen
        INSERT INTO albumreleases (albumreleases.Bands_Id, albumreleases.Albums_Id)
        VALUES (bands_Id, LAST_INSERT_ID());
    COMMIT;
END$$
DELIMITER ;