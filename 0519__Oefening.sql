use modernways;
ALTER TABLE liedjes
ADD COLUMN Genre varchar(20) char set utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE liedjes SET genre = 'Hard Rock'
WHERE artiest = 'Led Zeppelin' or
	  artiest = 'Van Halen';
SET SQL_SAFE_UPDATES = 1;