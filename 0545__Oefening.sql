USE modernways;
SELECT Artiest, SUM(Aantalbeluisteringen) AS "Totaal aantal beluisteringen"
FROM liedjes
GROUP BY artiest
HAVING MIN(length(artiest)) > 10 AND SUM(aantalbeluisteringen) > 100