USE modernways;

SELECT DISTINCT platformen.Naam AS 'Platform'
FROM releases
RIGHT JOIN platformen ON releases.platformen_id = platformen.id
LEFT JOIN games ON releases.games_id = games.id
WHERE games_id IS NOT NULL