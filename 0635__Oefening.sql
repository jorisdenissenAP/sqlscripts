USE modernways;
SELECT Leeftijdscategorie, AVG(loon)
FROM (SELECT id, voornaam, FLOOR(leeftijd / 10) * 10 AS Leeftijdscategorie
	  FROM personeelsleden) AS Leeftijdscategorieën
INNER JOIN personeelsleden
ON Leeftijdscategorieën.id = personeelsleden.id
GROUP BY Leeftijdscategorie