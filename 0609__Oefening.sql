USE Modernways;

SELECT Games.Titel AS Game, platformen.naam AS Platform
FROM releases
RIGHT JOIN Games ON releases.Games_Id = games.id
LEFT JOIN Platformen ON releases.Platformen_Id = platformen.id