USE aptunes;
DELIMITER $$
-- Deze procedure gaat lidmaatschappen verwijderen uit de database die voorbij een bepaalde datum zijn verstreken
-- Daarna geeft hij het aantal verwijderde lidmaatschappen terug
CREATE PROCEDURE CleanupOldMemberships(IN dasDate DATE, OUT amountDeleted INT)
BEGIN
	START TRANSACTION;
		SET SQL_SAFE_UPDATES = 0;
        -- Telt hoeveel lidmaatschappen over een bepaalde datum zijn gegaan en telt ze op in een variabele
		SELECT COUNT(*) INTO amountDeleted
		FROM Lidmaatschappen
		WHERE Einddatum IS NOT NULL AND Einddatum < dasDate;
        -- Verwijderd de lidmaatschappen uit de database
		DELETE
		FROM Lidmaatschappen
		WHERE Einddatum IS NOT NULL AND Einddatum < dasDate;
		SET SQL_SAFE_UPDATES = 1;
    COMMIT;
END$$
DELIMITER ;